#include "mem.h"
#include "util.h"


void test_successful_allocation() {
    heap_init(4096);

    void *ptr = _malloc(100);
    if (ptr) {
        debug("Allocated block: %p\n", ptr);
        debug_heap(stdout, HEAP_START);
        _free(ptr); // Освобождение блока
        debug("Freed block: %p\n", ptr);
        debug_heap(stdout, HEAP_START);
    } else {
        err("Memory allocation failed.\n");
    }

    heap_term();
}


void test_free_two_of_multiple() {
    heap_init(4096);

    void *ptr1 = _malloc(50);
    void *ptr2 = _malloc(50);

    if (ptr1 && ptr2) {
        debug_heap(stdout, HEAP_START);

        _free(ptr1);
        debug("Freed block: %p\n", ptr1);
        debug_heap(stdout, HEAP_START);

        _free(ptr2);
        debug("Freed block: %p\n", ptr2);
        debug_heap(stdout, HEAP_START);
    } else {
        err("Memory allocation failed.\n");
    }

    heap_term();
}


void test_extend_old_region() {
    heap_init(4096);

    void *ptr1 = _malloc(3000);
    void *ptr2 = _malloc(4000);

    if (ptr1 && ptr2) {
        debug_heap(stdout, HEAP_START);

        _free(ptr1);
        debug("Freed block: %p\n", ptr1);
        debug_heap(stdout, HEAP_START);

        _free(ptr2);
        debug("Freed block: %p\n", ptr2);
        debug_heap(stdout, HEAP_START);

        void *ptr3 = _malloc(3000);
        if (ptr3) {
            debug("Allocated block: %p\n", ptr3);
            debug_heap(stdout, HEAP_START);
            _free(ptr3);
            debug("Freed block: %p\n", ptr3);
            debug_heap(stdout, HEAP_START);
        } else {
            err("Memory allocation failed.\n");
        }
    } else {
        err("Memory allocation failed.\n");
    }

    heap_term();
}


void test_new_region_elsewhere() {
    size_t initial_size = 4096;

    heap_init(initial_size);

    void *manual_allocation = mmap(HEAP_START + initial_size, initial_size, PROT_READ | PROT_WRITE,
                                   MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, -1, 0);

    if (manual_allocation == MAP_FAILED) {
        err("Manual allocation failed");
        return;
    }


    void *ptr1 = _malloc(2000);
    void *ptr2 = _malloc(1000);

    if (ptr1 && ptr2) {
        debug_heap(stdout, HEAP_START);

        _free(ptr1);
        debug("Freed block: %p\n", ptr1);
        debug_heap(stdout, HEAP_START);

        _free(ptr2);
        debug("Freed block: %p\n", ptr2);
        debug_heap(stdout, HEAP_START);

        void *ptr3 = _malloc(5000);
        if (ptr3) {
            debug("Allocated block: %p\n", ptr3);
            debug_heap(stdout, HEAP_START);
            _free(ptr3);
            debug("Freed block: %p\n", ptr3);
            debug_heap(stdout, HEAP_START);
        } else {
            err("Memory allocation failed.\n");
        }
    } else {
        err("Memory allocation failed.\n");
    }

    munmap(HEAP_START + initial_size, initial_size);

    heap_term();
}


int main() {
    test_successful_allocation();
    test_free_two_of_multiple();
    test_extend_old_region();
    test_new_region_elsewhere();

    return 0;
}

#define _DEFAULT_SOURCE

#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"


void debug_block(struct block_header *b, const char *fmt, ...);

extern inline block_size size_from_capacity(block_capacity cap);
extern inline block_capacity capacity_from_size(block_size sz);

static bool block_is_big_enough(size_t query, struct block_header *block) {
    return block->capacity.bytes >= query;
}

static size_t pages_count(size_t mem) {
    return mem / getpagesize() + ((mem % getpagesize()) > 0);
}

static size_t round_pages(size_t mem) {
    return getpagesize() * pages_count(mem);
}

static void block_init(void *restrict addr, block_size block_sz, void *restrict next) {
    *((struct block_header *)addr) = (struct block_header){
            .next = next,
            .capacity = capacity_from_size(block_sz),
            .is_free = true
    };
}

static size_t region_actual_size(size_t query) {
    return size_max(round_pages(query), REGION_MIN_SIZE);
}

extern inline bool region_is_invalid(const struct region *r);

static void *map_pages(void const *addr, size_t length, int additional_flags) {
    return mmap((void *)addr,length,
                PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, -1, 0);
}


// 1
// Аллоцировать регион памяти и инициализировать его блоком
static struct region alloc_region(void const *addr, size_t query) {
    // Вычисляем фактический размер, который учитывает заголовок блока
    size_t actual_size = region_actual_size(size_from_capacity((block_capacity){.bytes = query}).bytes);

    // Пытаемся аллоцировать память с фиксированным адресом
    void *allocated_addr = map_pages(addr, actual_size, MAP_FIXED);
    if (allocated_addr == MAP_FAILED) {
        // Если не удалось с фиксированным адресом, пытаемся аллоцировать память без фиксации
        allocated_addr = map_pages(addr, actual_size, 0);
        if (allocated_addr == MAP_FAILED) {
            return REGION_INVALID; // Возвращаем REGION_INVALID, если не удалось аллоцировать память
        }
    }

    // Инициализируем блок и получаем указатель на блоковый заголовок
    struct block_header *block = allocated_addr;
    block_init(block, (block_size){.bytes = actual_size}, NULL);

    // Создаем структуру region с информацией о выделенном регионе
    struct region result = {.addr = allocated_addr, .size = actual_size, .extends = allocated_addr == addr};
    return result;
}


static void *block_after(struct block_header const *block);

void *heap_init(size_t initial) {
    const struct region region = alloc_region(HEAP_START, initial);
    if (region_is_invalid(&region))
        return NULL;

    return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/* --- Разделение блоков (если найденный свободный блок слишком большой)--- */
static bool block_splittable(struct block_header *restrict block, size_t query) {
    return block->is_free && query + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}


// 2
// Разделить блок, если его размер слишком большой для запроса выделения
static bool split_if_too_big(struct block_header *block, size_t query) {
    // Проверяем, можно ли разделить блок
    if (!block_splittable(block, query)) {
        return false;
    }

    // Вычисляем размер оставшейся части блока после выделения нужного размера
    size_t remaining_size = block->capacity.bytes - (query + offsetof(struct block_header, contents));

    // Вычисляем адрес следующего блока
    void *next_block_addr = block->contents + query;

    // Вычисляем размер оставшейся части блока
    block_size remaining_block_size = size_from_capacity((block_capacity){remaining_size});

    // Инициализируем следующий блок
    block_init(next_block_addr, remaining_block_size, block->next);

    // Обновляем размер текущего блока и его указатель на следующий блок
    block->capacity.bytes = query;
    block->next = next_block_addr;

    return true;
}



/* --- Слияние соседних свободных блоков --- */
static void *block_after(struct block_header const *block) {
    return (void *)(block->contents + block->capacity.bytes);
}

static bool blocks_continuous(struct block_header const *fst, struct block_header const *snd) {
    return (void *)snd == block_after(fst);
}

static bool mergeable(struct block_header const *restrict fst, struct block_header const *restrict snd) {
    return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}


// 3
// Освободить всю память, выделенную под кучу
void heap_term() {
    // Начинаем обход блоков, начиная с начального адреса кучи
    struct block_header *current = (struct block_header *)HEAP_START;

    while (current != NULL) {
        struct block_header *next = current->next;

        // Объединяем соседние блоки с текущим блоком перед вызовом munmap
        while (next != NULL && blocks_continuous(current, next)) {
            // Увеличиваем размер текущего блока на размер следующего
            current->capacity.bytes += size_from_capacity(next->capacity).bytes;

            // Пропускаем объединенные блоки
            current->next = next->next;
            next = next->next;
        }

        // Получаем размер текущего блока
        size_t block_size = size_from_capacity(current->capacity).bytes;

        // Вызываем munmap для освобождения памяти блока в куче
        if (munmap(current, block_size) == -1) {
            // Обработка ошибки при вызове munmap
            exit(EXIT_FAILURE);
        }

        // Переходим к следующему блоку в списке
        current = next;
    }
}


// 4
// Попытаться объединить текущий блок с последующим блоком, если они могут быть объединены
static bool try_merge_with_next(struct block_header *block) {
    // Получаем указатель на следующий блок
    struct block_header *next_block = block->next;

    // Проверяем, существует ли следующий блок и могут ли они быть объединены
    if (!next_block || !mergeable(block, next_block)) {
        return false; // Невозможно объединить блоки
    }

    // Увеличиваем размер текущего блока на размер следующего
    block->capacity.bytes += size_from_capacity(next_block->capacity).bytes;

    // Пропускаем объединенный блок в списке
    block->next = next_block->next;

    return true;
}


/* --- ... если размера кучи хватает --- */
struct block_search_result {
    enum { BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED } type;
    struct block_header *block;
};


// 5
// Найти подходящий блок или последний блок в связанном списке блоков
static struct block_search_result find_good_or_last(struct block_header *block, size_t sz) {
    struct block_header *last_block = NULL;

    // Проходим по связанному списку блоков
    for (struct block_header *current = block; current; current = current->next) {
        // Объединяем текущий блок с соседними блоками
        while (try_merge_with_next(current));

        // Проверяем, является ли блок подходящим
        if (current->is_free && block_is_big_enough(sz, current)) {
            // Возвращаем найденный подходящий блок
            return (struct block_search_result){.type = BSR_FOUND_GOOD_BLOCK, .block = current};
        }

        // Обновляем указатель на последний блок
        last_block = current;
    }

    // Возвращаем результат, указывающий, что подходящий блок не был найден
    return (struct block_search_result){.type = BSR_REACHED_END_NOT_FOUND, .block = last_block};
}


// 6
// Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
// Можно переиспользовать как только кучу расширили.
static struct block_search_result try_memalloc_existing(size_t query, struct block_header *block) {
    // Поиск подходящего блока или последнего блока в списке
    struct block_search_result result = find_good_or_last(block, query);

    // Проверка, найден ли подходящий блок
    if (result.type == BSR_FOUND_GOOD_BLOCK) {
        // Попытка разделить блок, если он слишком большой
        if (split_if_too_big(result.block, query)) {
            // Установка флага блока как "не свободен"
            result.block->is_free = false;
        }
    }

    return result;
}


// 7
// Увеличить размер кучи, выделяя новый регион памяти
static struct block_header *grow_heap(struct block_header *last, size_t query) {
    // Выделяем новый регион памяти
    struct region region = alloc_region(block_after(last), query);

    // Проверяем успешность выделения региона
    if (region_is_invalid(&region)) {
        return NULL;
    }

    // Устанавливаем указатель на следующий блок последнего блока
    last->next = region.addr;

    // Попытка объединить последний блок с новым блоком
    if (try_merge_with_next(last)) {
        return last;
    } else {
        return last->next;
    }
}



// 8
// Реализует основную логику malloc и возвращает заголовок выделенного блока
static struct block_header *memalloc(size_t query, struct block_header *heap_start) {
    // Уточняем минимальный размер запроса
    query = size_max(query, BLOCK_MIN_CAPACITY);

    // Пытаемся выделить память в существующем блоке
    struct block_search_result result = try_memalloc_existing(query, heap_start);

    if (result.type == BSR_FOUND_GOOD_BLOCK) {
        // Если удалось выделить память в существующем блоке, устанавливаем флаг "не свободен" и возвращаем блок
        result.block->is_free = false;
        return result.block;
    } else if (result.type == BSR_REACHED_END_NOT_FOUND) {
        // Если подходящий блок не найден, расширяем кучу
        struct block_header *last_block = result.block;
        // Попытка расширения кучи и получения нового заголовка блока
        struct block_header *new_block = grow_heap(last_block, query);
        if (new_block == NULL) return NULL;
        // Пытаемся снова выделить память в новом блоке
        result = try_memalloc_existing(query, new_block);
        return result.block;
    } else {
        // BSR_CORRUPTED: Это указывает на поврежденное состояние
        return NULL;
    }
}


void *_malloc(size_t query) {
    struct block_header *const addr = memalloc(query, (struct block_header *)HEAP_START);
    if (addr)
        return addr->contents;
    else
        return NULL;
}

static struct block_header *block_get_header(void *contents) {
    return (struct block_header *)(((uint8_t *)contents) - offsetof(struct block_header, contents));
}


// 9
// Освобождает блок памяти, устанавливая флаг "свободен" в его заголовке.
// Затем производит объединение с последующими свободными блоками.
void _free(void *mem) {
    if (!mem) return;

    // Получаем указатель на заголовок блока по переданному указателю памяти
    struct block_header *header = block_get_header(mem);

    // Устанавливаем флаг "свободен" в заголовке блока
    header->is_free = true;

    while (try_merge_with_next(header)) {
        // Продолжаем объединение с последующими блоками, пока это возможно
    }
}
